let gulp    = require('gulp'),
    rupture = require('rupture'),
    stylus  = require('gulp-stylus');

gulp.task('stylus', function () {
  return gulp.src(['./assets/stylus/*.styl','!./assets/stylus/_*.styl'])
           .pipe(stylus({
             use: [rupture()]
           }))
           .pipe(gulp.dest('./assets/css/'));
});

gulp.task('default', function() {
    gulp.watch(['./assets/stylus/*.styl'],['stylus']);
})
